#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd

# def load_dataset(strukturparametername, eingabeparametername):
#     pv_module = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="PV_Module")
#     stromspeicher = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Stromspeicher")
#     wechselrichter = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Hybrid-Wechselrichter")
#     strukturparameter = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Strukturparameter")
#     eingabeparameter = pd.read_excel("TOR_PV_Gruppenprojekt_Daten.ods", sheet_name="Eingabeparameter")
    
    
#     pv_module = pv_module.set_index(['Modell'], drop=True).to_dict(orient='index')
#     stromspeicher = stromspeicher.set_index(['Modell'], drop=True).to_dict(orient='index')
#     wechselrichter = wechselrichter.set_index(['Modell'], drop=True).to_dict(orient='index')
#     strukturparameter = strukturparameter.set_index(['Parametergruppe'], drop=True)
#     eingabeparameter = eingabeparameter.set_index(['Name'], drop=True)
    
#     #%% Struktur- und Eingabeparameter auswählen
#     strukturparameter = strukturparameter.loc[strukturparametername].to_dict()
#     eingabeparameter = eingabeparameter.loc[eingabeparametername].to_dict()
    
#     #%%

    
#     data = {
#             None:{
#                 "Panels" : {None : list(pv_module)},
#                 "Speicher" : {None : list(stromspeicher)},
#                 "Wechselrichter" : {None : list(wechselrichter)},
#                 "panelkosten" : {panelname : pv_module[panelname]
#                                  ["Preis pro Modul [€]"] for panelname in pv_module},
#                 "panelleistung" : {panelname : pv_module[panelname]
#                                    ["Maximalleistung [kWp]"] for panelname in pv_module},
#                 "panellaenge" : {panelname : pv_module[panelname]
#                                  ["Länge [m]"] for panelname in pv_module},
#                 "panelbreite" : {panelname : pv_module[panelname]
#                                  ["Breite [m]"] for panelname in pv_module},
#                 "speicherkapazitaet" : {speichername : stromspeicher[speichername]
#                                  ["Nennkapazität [kWh]"] for speichername in stromspeicher},
#                 "speicherkosten" : {speichername : stromspeicher[speichername]
#                                  ["Preis [€]"] for speichername in stromspeicher},
#                   }
#             }
    
#     params = {"Strukturparameter": strukturparameter,
#               "Eingabeparameter" : eingabeparameter
#               }
    
#     return data, params


def load_lookuptable(stromverbrauch, max_panels, investitionsvolumen):
    index_namen=["Jahresverbrauch [kWh]","Jahresertrag [kWh]", "Speicherkapazität [kWh]",
                 "Summe Stromkauf [kWh]", "Summe Stromverkauf [kWh]", "Kosten Speichermodul [€]",
                 "Anzahl der PV-Module [-]", "Gesamtflächer aller installierten PV-Module [m^2]",
                 "Preis aller installierten PV-Module [€]", "Wechselrichterkosten [€]"]
    data = pd.read_csv("skript_output_final.csv")
    data.index =index_namen
    data = data.T

    # Alle Kombinationen mit andern Stromverbrauch rauswerfen
    data = data.loc[data["Jahresverbrauch [kWh]"]== stromverbrauch]

    # Montagekosten hinzufügen:
    data["Preis aller installierten PV-Module [€]"] = data["Preis aller installierten PV-Module [€]"] + 100 * data["Anzahl der PV-Module [-]"]

    data_dict = {
          None:{
              "f" : {None : list(data.index)},
              "jahresverbrauch" : data["Jahresverbrauch [kWh]"].to_dict(),
              "jahresertrag" : data["Jahresertrag [kWh]"].to_dict(),
              "speicherkapazitaet" : data["Speicherkapazität [kWh]"].to_dict(),
              "kauf" : data["Summe Stromkauf [kWh]"].to_dict(),
              "verkauf" : data["Summe Stromverkauf [kWh]"].to_dict(),
              "speicherkosten" : data["Kosten Speichermodul [€]"].to_dict(),
              "anzahl" : data["Anzahl der PV-Module [-]"].to_dict(),
              "panelflaeche" : data["Gesamtflächer aller installierten PV-Module [m^2]"].to_dict(),
              "panelkosten" : data["Preis aller installierten PV-Module [€]"].to_dict(),
              "wechselrichterkosten" : data["Wechselrichterkosten [€]"].to_dict()
          }  
          }
    return data_dict
