#!/usr/bin/env python
# coding: utf-8
#---------------------------------------------------------------------#

# Importiere benötigte Bibliotheken:
import pandas as pd
from preprocessing import *
import pyomo.environ as pyo


# Definition unveränderlicher Werte
laufzeit            = 20    # in Jahren
kaufpreis           = 0.32  # €/kwh
verkaufspreis       = 0.07  # €/kwh
panellaenge         = 1.72  # in m
panelbreite         = 1.35  # in m
wechselrichter_kosten = 1500# in €

# Input Daten
stromverbrauch      = 4500  # pro Jahr in kwh [1500 2500 3500 4500]
dachlaenge          = 7  # in m
dachbreite          = 12  # in m
dachflaeche         = dachlaenge * dachbreite   # in m^2
investitionsvolumen = 20000 # in €

investitionsvolumen = investitionsvolumen - wechselrichter_kosten
max_panels = (dachlaenge//panellaenge)*(dachbreite//panelbreite)


# laden der Parameter
# rauswerfen von nicht passenden Kombinationen
# Erstellung des dicts
Data = load_lookuptable(stromverbrauch, max_panels, investitionsvolumen)

# Jahre in denen Geld für den Verkauf von Strom eingenommen werden kann:
if laufzeit>20:
    verkaufzeit = 20
else:
    verkaufzeit = laufzeit





# Erzeuge Model:
model = pyo.AbstractModel()

# Set
model.f = pyo.Set()

# Parameter (Struktur nach Tabelle csv?)
model.jahreverbrauch     = pyo.Param(model.f)
model.jahresertrag       = pyo.Param(model.f)
model.speicherkapazitaet = pyo.Param(model.f)
model.kauf               = pyo.Param(model.f) # Summe der zu kaufenden Energie der Kombination i
model.verkauf            = pyo.Param(model.f) # Summe der zu verkaufenden Energie der kombination i
model.speicherkosten     = pyo.Param(model.f) # Preis für den Stromspeicher der Kombination i
model.anzahl             = pyo.Param(model.f)
model.panelflaeche       = pyo.Param(model.f) # Panelfläche aller Panel der Kobination i
model.panelkosten        = pyo.Param(model.f) # Preis für alle Panel der Kombination i


# Variable
model.x = pyo.Var(model.f, domain=pyo.Binary)
# model.y = pyo.Var(model.f, domain = pyo.NonNegativeIntegers) #Zählvariable für Anzahl Panels
# model.z = pyo.Var(model.f, domain = pyo.NonNegativeIntegers) #Zählvariable für Anzahl Panels in Breitenrichtung


# Randbedingungen
# Nicht mehr als eine Kombination darf ausgewählt werden
def rule_kombinationen(model):
    return sum(model.x[i] for i in model.f) == 1                   #Hier für sum als Basis model.f nehmen?
model.r1 = pyo.Constraint( rule = rule_kombinationen)

# Das Investitionsvolumen darf nicht überschritten werden
def rule_investition(model):
    return sum(model.x[i]*(model.panelkosten[i] + model.speicherkosten[i]) for i in model.f) <= investitionsvolumen
model.r2 = pyo.Constraint(rule = rule_investition)

# # Die Dachfläche darf nicht überschritten werden
# def rule_dachflaeche(model):
#     return sum(model.x[i] * model.panelflaeche[i] for i in model.n) <= dachflaeche
# model.r3 = pyo.Constraint(model.n, rule = rule_dachflaeche)


# Für jede PV-Anlage berechetes max_panels als einzelnes Constraint
def rule_maxpanels(model):
    return sum(model.x[i]*model.anzahl[i] for i in model.f) <= max_panels
model.r3 = pyo.Constraint(rule = rule_maxpanels)

# #Alternative für Berechnung Anzahl Panels: 
# #(oben als Eingangsvariablen dann Variablen für panellaenge und panelbreite definieren)
# #Anzahl der Panels anpassen an Länge des Daches:
# def rule_dachlaenge(model):
#     return sum (model.x[i]*model.y[i]*panellaenge for i in model.f) <= dachlaenge
# model.r4 = pyo.Constraint(model.f, rule = rule_dachlaenge)

# #Anzahl der Panels anpassen an Breite des Daches:
# def rule_dachbreite(model):
#     return sum(model.x[i]*model.z[i]*panelbreite for i in model.f) <= dachbreite
# model.r5 = pyo.Constraint(model.f, rule = rule_dachbreite)

# #Begrenzung nur ein y > 0:
# def rule_y_begr(model, i):
#     return model.y[i] <= model.x[i]*100  #big M
# model.r6 = pyo.Constraint(model.f, rule = rule_y_begr)

# #Begrenzung nur ein z > 0:
# def rule_z_begr(model, i):
#     return model.z[i] <= model.x[i]*100  #big M
# model.r7 = pyo.Constraint(model.f, rule = rule_z_begr)

# #Begrenzung in Auswahl in Tabelle:
# def rule_dachflaeche(model):
#     return sum(model.x[i]*model.y[i]*model.z[i]*206 for i in model.f) >= sum(model.x[i]*(model.panelkosten[i]/206) for i in model.f) #geht auch ohne sum?
# model.r7 = pyo.Constraint(model.f, rule = rule_dachflaeche)




# Zielfunktion kosten minimieren
def obj(model):
    return sum(model.x[i]*(model.panelkosten[i] + model.speicherkosten[i] - laufzeit * model.kauf[i] * kaufpreis - laufzeit * model.verkauf[i] * verkaufspreis)for i in model.f)
model.objective = pyo.Objective(rule = obj, sense = pyo.minimize)


#Erzeuge Instanz:
instance1 = model.create_instance(Data)


#Solver einbinden und Lösen des Optimierungsprogramms:
import os
os.environ["NEOS_EMAIL"] = "akudfbahjkbfvjahd@stud.tu-darmstadt.de"
solver = pyo.SolverManagerFactory("neos")
results = solver.solve(instance1, tee = True, opt = "cplex")

#Ausgabe der Ergebnisse:
for i in instance1.f:
    if int(instance1.x[i].value) == 1:
        Auswahl = i
        print("Kombination %s wurde gewählt" % str(i)[13:])

print("Kosten nach Laufzeitende = %.2f€" % (pyo.value(instance1.objective) + wechselrichter_kosten))
print("Es werden %s Panels montiert" % str(instance1.anzahl[Auswahl]))
print("Jahresertrag = %s kWh" % instance1.jahresertrag[Auswahl])
print("Speicherkapazität = %s kWh" % instance1.speicherkapazitaet[Auswahl])
print("Jährlich gekaufter Strom = %.3f kWh" % -instance1.kauf[Auswahl])
print("Jährlich verkaufter Strom = %.3f kWh" % instance1.verkauf[Auswahl])
print("Die Investitionskosten zu Beginn betragen %.2f€ (Speicher = %.2f€, Wechselrichter = 1500€, Panelkosen = %.2f€" % ((instance1.speicherkosten[Auswahl]+instance1.panelkosten[Auswahl]+1500), instance1.speicherkosten[Auswahl], instance1.panelkosten[Auswahl]))

# Abschließende Berechnungen:
print("\n")

# Voraussichtliche Kosten ohne PV-Anlage:
kosten_ohne_PV = laufzeit*stromverbrauch*kaufpreis
print("Die Kosten ohne PV-Anlage würden sich voraussichtlich auf %.2f€ belaufen" % kosten_ohne_PV)

# gesamtleistung
gesamtleistung = instance1.anzahl[Auswahl] * 0.365   # Gesamtleistung in kWp
print("Die installierte Gesamtleistung der PV-Anlage beträgt %s kWp" % str(gesamtleistung))
