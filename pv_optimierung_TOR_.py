#!/usr/bin/env python
# coding: utf-8
#---------------------------------------------------------------------#

# Importiere benötigte Bibliotheken:
import pandas as pd
from preprocessing import *
import pyomo.environ as pyo



# Definition unveränderlicher Werte
kaufpreis             = 0.32                      # €/kwh
verkaufspreis         = 0.07                      # €/kwh
panellaenge           = 1.72                      # in m
panelbreite           = 1.35                      # in m
wechselrichter_kosten = 1500                      # in €

# Input Daten
laufzeit              = 30                        # in Jahren
personen              = 2                         # Anzahl Personen im Haushalt (1-18)
dachlaenge            = 7                         # in m
dachbreite            = 10                        # in m 
investitionsvolumen   = 15000                     # in €



# Preprocessing
max_panels = (dachlaenge//panellaenge)*(dachbreite//panelbreite)
dachflaeche         = dachlaenge * dachbreite                               # in m^2
stromverbrauch        = 1500 + (1000 * (personen - 1))                      # pro Jahr in kwh [1500 2500 3500 4500 ...]

# Jahre in denen Geld für den Verkauf von Strom eingenommen werden kann (20 Jahre nach EEG):
if laufzeit>20:
    verkaufzeit = 20
else:
    verkaufzeit = laufzeit

# laden der Parameter
Data = load_lookuptable(stromverbrauch, max_panels, investitionsvolumen)



# Erzeuge Model:
model = pyo.AbstractModel()

# Set
model.f = pyo.Set()

# Parameter (Struktur nach Tabelle csv?)
model.jahresverbrauch      = pyo.Param(model.f) # Jahresverbrauch in kWh der Kombination i
model.jahresertrag         = pyo.Param(model.f) # Jahresertrag der Anlage in Kombination i
model.speicherkapazitaet   = pyo.Param(model.f) # Speicherkapazität in kWh der Kombination i
model.kauf                 = pyo.Param(model.f) # Summe der zu kaufenden Energie der Kombination i
model.verkauf              = pyo.Param(model.f) # Summe der zu verkaufenden Energie der kombination i
model.speicherkosten       = pyo.Param(model.f) # Preis für den Stromspeicher der Kombination i
model.anzahl               = pyo.Param(model.f) # Anzahl der gekauften Panels
model.panelflaeche         = pyo.Param(model.f) # Panelfläche aller Panel der Kobination i
model.panelkosten          = pyo.Param(model.f) # Preis für alle Panel der Kombination i
model.wechselrichterkosten = pyo.Param(model.f) # Preis für den Wechselrichter

# Variablen
model.x = pyo.Var(model.f, domain=pyo.Binary)




# Randbedingungen
# Nicht mehr als eine Kombination darf ausgewählt werden
def rule_kombinationen(model):
    return sum(model.x[i] for i in model.f) == 1                  
model.r1 = pyo.Constraint( rule = rule_kombinationen)

# Das Investitionsvolumen darf nicht überschritten werden
def rule_investition(model):
    return sum(model.x[i]*(model.panelkosten[i] + model.speicherkosten[i] + model.wechselrichterkosten[i]) for i in model.f) <= investitionsvolumen
model.r2 = pyo.Constraint(rule = rule_investition)

# Für jede PV-Anlage berechetes max_panels als einzelnes Constraint
def rule_maxpanels(model):
    return sum(model.x[i]*model.anzahl[i] for i in model.f) <= max_panels
model.r3 = pyo.Constraint(rule = rule_maxpanels)



# Zielfunktion kosten minimieren
def obj(model):
    return sum(model.x[i]*(model.panelkosten[i] + model.speicherkosten[i] + model.wechselrichterkosten[i] - laufzeit * model.kauf[i] * kaufpreis - verkaufzeit * model.verkauf[i] * verkaufspreis)for i in model.f)
model.objective = pyo.Objective(rule = obj, sense = pyo.minimize)



#Erzeuge Instanz:
instance1 = model.create_instance(Data)


# Solver einbinden und Lösen des Optimierungsprogramms:
import os
os.environ["NEOS_EMAIL"] = "akudfbahjkbfvjahd@stud.tu-darmstadt.de"
solver = pyo.SolverManagerFactory("neos")
results = solver.solve(instance1, tee = True, opt = "cplex")

# Ausgabe der Ergebnisse:
for i in instance1.f:
    if int(instance1.x[i].value) == 1:
        Auswahl = i
        print("Kombination %s wurde gewählt" % str(i)[13:])

print("\n")


if instance1.anzahl[Auswahl] > 0:
    
    print("Es werden %s Panels montiert" % str(instance1.anzahl[Auswahl]))
    print("Speicherkapazität = %s kWh" % instance1.speicherkapazitaet[Auswahl])
    print("Jahresertrag = %s kWh" % instance1.jahresertrag[Auswahl])
    print("Jährlich zugekaufter Strom = %.3f kWh" % -instance1.kauf[Auswahl])
    print("Jährlich verkaufter Strom = %.3f kWh" % instance1.verkauf[Auswahl])

    # gesamtleistung:
    gesamtleistung = instance1.anzahl[Auswahl] * 0.365   # Gesamtleistung in kWp
    print("Die installierte Gesamtleistung der PV-Anlage beträgt %.2f kWp" % gesamtleistung)

    print("\n")
    print("Kosten nach Laufzeitende = %.2f€" % pyo.value(instance1.objective))
    print("Die Investitionskosten zu Beginn betragen %.2f€ (Speicher = %.2f€, Wechselrichter = %.2f€, Panelkosen = %.2f€)" % ((instance1.speicherkosten[Auswahl]+instance1.panelkosten[Auswahl]+instance1.wechselrichterkosten[Auswahl]), instance1.speicherkosten[Auswahl],instance1.wechselrichterkosten[Auswahl], instance1.panelkosten[Auswahl]))


    print("\n")


    # Voraussichtliche Kosten ohne PV-Anlage:
    kosten_ohne_PV = laufzeit*stromverbrauch*kaufpreis
    print("Die Kosten ohne PV-Anlage würden sich voraussichtlich auf %.2f€ belaufen" % kosten_ohne_PV)

    # Kosteneinsparung:
    if kosten_ohne_PV > float(pyo.value(instance1.objective)):
        ersparnis = kosten_ohne_PV - pyo.value(instance1.objective)
        print("Über die gesamte Laufzeit sparen Sie %.2f€ ein" % ersparnis)

        fixkosten = instance1.speicherkosten[Auswahl]+instance1.panelkosten[Auswahl]+instance1.wechselrichterkosten[Auswahl]
        amortisationsdauer = fixkosten / (stromverbrauch*kaufpreis - (float(pyo.value(-instance1.kauf[Auswahl]*kaufpreis)) + float(pyo.value(-instance1.verkauf[Auswahl]*verkaufspreis))))
        print("Die Amortisationsdauer beträgt ca. %.2f Jahre" % amortisationsdauer)
        
    print("\n")
    print("\n")

else:
    print("Die Anschaffung einer PV-Anlage mit den von Ihnen eingegebenen Parametern wäre nicht rentabel!")